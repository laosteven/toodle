# Toodle: Learning Platform for High School students #
-------

Conceive a **dynamical prototype** of a mobile application made for *high school students* with the following objectives:

* Receive updates for new evaluation reports;
* Send and receive emails;
* **Submit assignments** via integrated `Dropbox`, `Google Drive` or `Github` links;
* Check class schedule.

The purpose of the class is to *conceptualize, design, test* and then *analyse* the results to bring improvement to the current design before coding.
That being said, there is absolutely **no code** in this project. The first step of the project is to develop a static prototype with **[Balsamiq Mockups](https://balsamiq.com/products/mockups/)**:

##  ##
![Accueil.png](https://bitbucket.org/repo/6kXM44/images/3919704289-Accueil.png)

Once the static prototype has been made, it is safe to proceed with **[Axure](http://www.axure.com/)**, a dynamical prototyping tool:

# Home page #
-------
![acc1.png](https://bitbucket.org/repo/6kXM44/images/3540138182-acc1.png)

**User content**

Displays the student's **full name**, **permanent code** and his **profile picture**.

* Confirms that the user is using the correct account.
* Underneath the user information are the **current notification alerts**. 

####
**Scores**

Shows recently updated assignments and exams.

* By default, this section will display the **title** of the assignment / exam, the **weighting** and the student's **score**
* By pressing the Info icon, the student may view class average, percentile ranks, etc.

# Class page #
-------
![1.png](https://bitbucket.org/repo/6kXM44/images/38931352-1.png)

**Class list**

View all current and previous classes. 

Red **notification badges** will be displayed on the top-right corner of the class label which represents updated scores about homework, quizzes or exams.

####
![2.png](https://bitbucket.org/repo/6kXM44/images/2798138518-2.png)

**Submitting an assignment (part 1)**

Upon inspecting a class' sub-page, it is possible to navigate in the **Submit** (*Remises*) tab and choose an assignment in the list. 

The deadline is also included below the assignment label.

####
![3.png](https://bitbucket.org/repo/6kXM44/images/340802475-3.png)

**Submitting an assignment (part 2)**

By selecting an assignment, another sub-page will appear and will allow you:

* to add your **teammates**;
* to choose the hosting website: either `Drive`, `Dropbox` or `Github` depending on the context of the assignment;
* to specify the **hyperlink** of file you want to submit*.

**This is a concept idea. It requires a moderate amount of management skills to control folder accesses and permissions.*

# Mailing page #
-------
![acc3.png](https://bitbucket.org/repo/6kXM44/images/3659739779-acc3.png)

**Mailing list**

Display the whole **mailing list** of the student about upcoming school activities, work assignments, teamwork discussion, etc.

* The **gear** icon allows the user to *check* their folders and *create* new categories.
* The **hourglass** icon toggles a search bar to *filter* specific keywords in mail contents.
* The **pencil** icon enables the user to *compose* a new message.
* Selecting emails can be done by clicking on their **checkbox**: users may add them to their **favorite** folder or **delete** them.

The name **Toodle** is actually a spin-off of the school's learning platform, **[Moodle](https://moodle.org/)**.

# Objectives #
-------
* Learn methods to make the mobile application easier to navigate without being confused.
* Adopt long term planification before making a final version to avoid spending money on corrections.

# How do I use the files? #
-------
* To view `bmpr` files, use **[Balsamiq Mockups](https://balsamiq.com/products/mockups/)**.
* To view `rp` files, use **[Axure](http://www.axure.com/)**.

# For more information #
-------
Visit the following website: [**User interface conception and evaluation** (GTI350)](https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=gti350) [*fr*]